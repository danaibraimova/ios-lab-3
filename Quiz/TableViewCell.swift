//
//  TableViewCell.swift
//  Quiz
//
//  Created by Saulebekov Azamat on 13.09.17.
//  Copyright © 2017 Saulebekov Azamat. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var question: UILabel!
    
    
    @IBOutlet weak var currentAns: UILabel!

    
    @IBOutlet weak var correctAns: UILabel!
    
    func Color(tORf: Bool){
        if(tORf == true){
            self.backgroundColor = #colorLiteral(red: 0.7687553737, green: 1, blue: 0.809534248, alpha: 1)
        }
        else{
            self.backgroundColor = #colorLiteral(red: 1, green: 0.568676069, blue: 0.619528486, alpha: 1)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
